package com.ExamenDispMov.DiegoViscarra.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.ExamenDispMov.DiegoViscarra.data.BookDatabase
import com.ExamenDispMov.DiegoViscarra.repository.BookRepository
import com.ExamenDispMov.DiegoViscarra.model.Book
import kotlinx.coroutines.launch
import kotlinx.coroutines.Dispatchers

class BookViewModel(application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<Book>>
    private val repository: BookRepository
    init{
        val bookDao= BookDatabase.getDatabase(
            application
        ).bookDao()
        repository = BookRepository(bookDao)
        readAllData = repository.readAllData
    }

    fun addBook (book: Book){
        viewModelScope.launch(Dispatchers.IO){
            repository.addBook(book)
        }
    }

    fun updateBook (book: Book){
        viewModelScope.launch (Dispatchers.IO){
            repository.updateBook(book)
        }
    }

    fun deleteBook (id: Int){
        viewModelScope.launch (Dispatchers.IO){
            repository.deleteBook(id)
        }
    }
}