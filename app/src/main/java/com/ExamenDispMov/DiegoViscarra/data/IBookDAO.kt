package com.ExamenDispMov.DiegoViscarra.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ExamenDispMov.DiegoViscarra.model.Book

@Dao
interface IBookDAO {

    @Insert(onConflict =  OnConflictStrategy.IGNORE)
    suspend fun addBook(book: Book)

    @Update
    suspend fun updateBook(book: Book)

    @Query("DELETE FROM book_table WHERE id= :id")
    fun deleteBook(id: Int)

    @Query( "SELECT * FROM book_table ORDER BY id ASC")
    fun getAllData(): LiveData<List<Book>>
}