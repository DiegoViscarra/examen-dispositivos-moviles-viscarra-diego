package com.ExamenDispMov.DiegoViscarra.repository

import androidx.lifecycle.LiveData
import com.ExamenDispMov.DiegoViscarra.data.IBookDAO
import com.ExamenDispMov.DiegoViscarra.model.Book

class BookRepository (private val bookDao: IBookDAO) {

    val readAllData: LiveData<List<Book>> = bookDao.getAllData()

    suspend fun  addBook  (book: Book){
        bookDao.addBook(book)
    }

    suspend fun updateBook(book: Book){
        bookDao.updateBook(book)
    }

    suspend fun deleteBook(id: Int){
        bookDao.deleteBook(id)
    }
}