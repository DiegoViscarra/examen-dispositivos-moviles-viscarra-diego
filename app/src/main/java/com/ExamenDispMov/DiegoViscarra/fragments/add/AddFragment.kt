package com.ExamenDispMov.DiegoViscarra.fragments.add

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.ExamenDispMov.DiegoViscarra.R
import com.ExamenDispMov.DiegoViscarra.model.Book
import com.ExamenDispMov.DiegoViscarra.viewmodel.BookViewModel
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*


class AddFragment : Fragment() {

    private lateinit var mBookViewModel: BookViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_add, container, false)
        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)

        view.addBook_btn.setOnClickListener{
            insertDataToDataBase()
        }
        return  view
    }

    private fun insertDataToDataBase() {
        val title = addTitle.text.toString()
        val isbn = addIsbn.text.toString()
        val author = addAuthor.text.toString()
        val publicationDate = addPublicationDate.text.toString()
        val pages = addPages.text
        val description = addDescription.text.toString()
        val photoUrl = addPhotoUrl.text.toString()

        if(inputCheck(title, isbn, author, publicationDate, pages, description, photoUrl)){
            val book = Book(
                0,
                title,
                isbn,
                author,
                publicationDate,
                Integer.parseInt(pages.toString()),
                description,
                photoUrl
            )
            mBookViewModel.addBook(book)
            Toast.makeText(requireContext(), "The Book was successfully created", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        }
        else {
            Toast.makeText(requireContext(), "All the fields need to be fill", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck (title:String, isb:String, author: String, publicationDate: String, pages: Editable, description: String, photoUrl: String): Boolean{
        return !(TextUtils.isEmpty(title) && TextUtils.isEmpty(isb) && TextUtils.isEmpty(author) && TextUtils.isEmpty(publicationDate) && TextUtils.isEmpty(description) && TextUtils.isEmpty(photoUrl) && pages.isEmpty())
    }


}