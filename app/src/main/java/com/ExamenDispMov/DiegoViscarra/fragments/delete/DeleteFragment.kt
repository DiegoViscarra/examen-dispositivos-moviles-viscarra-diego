package com.ExamenDispMov.DiegoViscarra.fragments.delete

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.ExamenDispMov.DiegoViscarra.R
import com.ExamenDispMov.DiegoViscarra.viewmodel.BookViewModel
import kotlinx.android.synthetic.main.fragment_delete.*
import kotlinx.android.synthetic.main.fragment_delete.view.*

class DeleteFragment : Fragment() {

    private lateinit var mBookViewModel : BookViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_delete, container, false)
        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)

        view.deleteBook_btn.setOnClickListener{
            deleteDataFromDataBase()
        }
        return  view
    }

    private fun deleteDataFromDataBase() {
        val id = Integer.parseInt(deleteId.text.toString())
        if(inputCheck(id)){
            mBookViewModel.deleteBook(id)
            Toast.makeText(requireContext(), "The Book was successfully deleted", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_deleteFragment_to_listFragment)
        }
        else {
            Toast.makeText(requireContext(), "The field needs to be fill", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck (id: Int): Boolean{
        return (id!=0)
    }
}