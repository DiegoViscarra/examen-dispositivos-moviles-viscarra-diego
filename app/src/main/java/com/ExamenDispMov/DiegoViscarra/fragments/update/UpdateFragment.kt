package com.ExamenDispMov.DiegoViscarra.fragments.update

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.ExamenDispMov.DiegoViscarra.R
import com.ExamenDispMov.DiegoViscarra.model.Book
import com.ExamenDispMov.DiegoViscarra.viewmodel.BookViewModel
import kotlinx.android.synthetic.main.fragment_update.*
import kotlinx.android.synthetic.main.fragment_update.view.*


class UpdateFragment : Fragment() {

    private val args by navArgs<UpdateFragmentArgs>()
    private lateinit var mBookViewModel :BookViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_update, container, false)
        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)

        view.updateTitle.setText(args.currentBook.title)
        view.updateIsbn.setText(args.currentBook.isbn)
        view.updateAuthor.setText(args.currentBook.author)
        view.updatePublicationDate.setText(args.currentBook.publicationDate)
        view.updatePages.setText(args.currentBook.pages.toString())
        view.updateDescription.setText(args.currentBook.description)
        view.updatePhotoUrl.setText(args.currentBook.photoUrl)

        view.updateBook_btn.setOnClickListener {
            updateItem()
        }

        return  view
    }

    private  fun updateItem(){
        val title = updateTitle.text.toString()
        val isbn = updateIsbn.text.toString()
        val author  = updateAuthor.text.toString()
        val publicationDate  = updatePublicationDate.text.toString()
        val pages = Integer.parseInt(updatePages.text.toString())
        val description = updateDescription.text.toString()
        val photoUrl = updatePhotoUrl.text.toString()

        if(inputCheck(title, isbn, author, publicationDate, updatePages.text, description, photoUrl)){
            val updateBook = Book (args.currentBook.id, title, isbn, author, publicationDate, pages, description, photoUrl)
            mBookViewModel.updateBook(updateBook)
            Toast.makeText(requireContext(), "The Book was successfully updated", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }else{
            Toast.makeText(requireContext(), "All the fields need to be fill", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck (title:String, isbn:String, author: String, publicationDate:String, pages: Editable, description: String, photoUrl: String): Boolean{
        return !(TextUtils.isEmpty(title) && TextUtils.isEmpty(isbn) && TextUtils.isEmpty(author) && TextUtils.isEmpty(publicationDate)  && TextUtils.isEmpty(description) && TextUtils.isEmpty(photoUrl) && pages.isEmpty())
    }
}